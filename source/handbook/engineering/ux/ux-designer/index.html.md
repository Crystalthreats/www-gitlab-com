---
layout: markdown_page
title: "Product Designer Workflow"
---

### On this page

{:.no_toc}

- TOC
{:toc}

## Product Designer

Like other departments at GitLab, we follow the [GitLab Workflow](/handbook/communication/#everything-starts-with-an-issue) and the [Engineering Workflow](/handbook/engineering/workflow/). Reviewing those resources is a good starting point for context on the Product Designer workflow below.

### Product Designer onboarding

If you are just starting out here at GitLab, welcome! Make sure to review all the pages here in the UX section of the handbook, they will help you get oriented. There is also a specific page dedicated to [Product Designer onboarding](/handbook/engineering/ux/uxdesigner-onboarding).

### Planning and managing work

Product designers are responsible for stage- and UX-team-assigned work. Product designers are responsible for working with their stage peers and managers, as needed, to manage their capacity and complete their work on target and on time.  Here are some guidelines to help Product Designers manage their work:

Product Designers: 
* Make sure discipline peers are aware of UX team assignments, such as UX OKRs, and what you'll need from them to succeed.
* Account for time off (both yours and others) as best you can during milestone planning.
* Let your manager know right away, if you believe you're not trending to complete your work. The sooner your manager knows, the higher the likelihood they can resolve the issue and manage expectations.

Managers:
* If requested by Product Designers, help them set a baseline capacity for stage- and UX-team-assigned work each milestone. Designers can use this information to balance work and manage expectations. 
* Quantify strategy work with clear time-to-complete (TTC) expectations, measurable goals, and deadlines.
* Resolve team questions, concerns, and blockers quickly.
* Make sure cross-functional partners are aware of UX OKRs and their associated dependencies.

### Working on issues

#### Scheduling of issues in a milestone
All issues in a milestone labeled [Deliverable](https://gitlab.com/groups/gitlab-org/issues?state=opened&label_name%5B%5D=Deliverable) that need UX will be assigned to a Product Designer by the kickoff. Issues labeled [Stretch](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Stretch) may or may not be assigned to a Product Designer by the kickoff.

#### Priority for UX issues
UX works on issues in the following order:
* Has the current release milestone and is labeled Deliverable
* [UX OKRs](/company/okrs/) 
* Has the current release milestone and is labeled Stretch
* Has the Pajamas label
* Has the next release milestone
* [Has the **Accepting merge requests label**](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&label_name[]=Accepting+merge+requests&label_name[]=UX)
* [Has milestone **Next 2-3 months**](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&milestone_title=Next+2-3+months&label_name[]=UX)
* [Has milestone **Next 3-6 months**](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&milestone_title=Next+3-6+months&label_name[]=UX)
* [Has milestone **Backlog**](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&milestone_title=Backlog&label_name[]=UX)
* [Popular or with high community involvement (number of comments or upvotes)](https://gitlab.com/groups/gitlab-org/issues?label_name[]=UX&sort=upvotes_desc&state=opened)
* [Everything else](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&label_name[]=UX)

#### Product design process
Define the opportunity:
* Work with your PM to [validate][product-dev-flow] *who* you're designing for, *what* you're designing, and *why* you're designing it.
* Help your PM express the who/what/why as a user story. For example, "As a (who), I want (what), so I can (why/value)." If you’re asked to implement a non-evidence-based how, then ask the PM to refocus on the who/what/why, so everyone can work together to find the best how. 
* Help your PM to define [MVC](/handbook/values/#minimum-viable-change-mvc) success criteria, prioritizing MVC “must-haves” and non-MVC “should-haves” and “could-haves.” (Note that this success criteria is subject to change based on new learning from the iterative design process and customer feedback.)

Before you design:
* Investigate whether there is [existing UX Research](/handbook/engineering/ux/ux-research#ux-research-archive) or data that could help inform your decisions and measure results. If there isn't, contact your UX Researcher to conduct (or guide you in conducting) research for the problem.
* Consider conducting competitive analysis to inform your work. Look for terminology, functionality, and UX conventions that can help refine success criteria. Only stray from industry conventions with strategic intent, such as capitalizing on [disruptive innovation](https://www.economist.com/the-economist-explains/2015/01/25/what-disruptive-innovation-means) opportunities. We want users to migrate from other tools to ours, and they’re more likely to be successful and satisfied when our products use conventions that are familiar based on other tools they've used.
* Consider creating user flows or journey maps to help ensure you've considered the entire workflow and also to help communicate that workflow to your team.

Ideate and iterate:
* Share design ideas in the lowest fidelity that still communicates your idea. To keep the cost of change low, only increase fidelity as design confidence grows and implementation requires. 
* Ask for feedback from your PM throughout the design process to help refine your understanding of the problem and solution criteria. 
* Ask for feedback from other UXers to help improve your work. At minimum, you'll get objective feedback and new ideas that lead to better solutions. You might also get context you didn’t know you were missing, such as GitLab-specific or industry-standard design conventions.
* Engage engineering peers early and often. Their insight into technical costs and feasibility is essential to determining viable designs and MVCs. Also, invite design feedback, especially if you’re solving for a development workflow they’re familiar with.
* [Validate][product-dev-flow] your proposed solution with customers/users.

Refine MVC:
* UX issues have a tendency to expand in scope. Work with your PM and developers to revisit which aspects of the design are “must haves” versus those that can be pushed until later. Document non-MVC concepts and research in new issues, marking the new issues as related to the original issue. If you’re ever unsure how to split apart large issues, work with your UX Manager.
* Developers should be able to build a working feature within one release. If a feature is too large to build within one release, work with your PM and Engineering team to determine the best way to split the feature into smaller segments.

Present an MVC solution:
* After you've validated your solution with users, propose just one solution. Proposing multiple alternative solutions for others to pick undermines your position as a UX expert and leads to design by committee. If you have a good reason to propose multiple alternative solutions, make sure to explain why.
* When sharing asynchronously in an issue, make sure your audience has the context necessary to understand your proposal and how they can help. Is it clear who will use the solution and what it will enable them to accomplish? Do you need feedback or assistance from stakeholders? If so, on what specifically?  Or, are you looking for approval? To make reviewing easier, have you highlighted things that changed since the last review?
* @mention your UX Manager on the issue for awareness and feedback.
* Frame design discussions around the customer and the problem being solved, not the UI or functionality itself. When presenting, start with the current state and how it fails to meet user needs, and then walk through the proposed solution from the user’s point of view. As the discussion unfolds, continually tie everything back to the user’s experience and needs.
* Anticipate questions that others might have, and try to answer them in your proposal comments. You don’t have to explain everything, but try to communicate a bit of your rationale every time you propose something. This is particularly important when proposing changes or challenging the status quo, because it reduces the feedback loop and time spent on unnecessary discussions. It also builds the UX Department’s credibility, because we deal with a lot of seemingly subjective issues.
* Keep the SSOT updated with what’s already agreed upon so that everyone can know where to look. This includes images or links to your design work.
* If you’re working with design files, follow the instructions in the [GitLab Design project contribution guidelines][gitlab-design-project-contribution-guidelines] and regularly commit them.
* If you are proposing a solution that will introduce a new UX paradigm, or change an existing one, please consider the following:
    1. Will this pattern be inconsistent with other areas of the application?
    1. Will other areas of the application need to be updated to match?
    1. Does this new pattern significantly improve the user experience?
    1. If you decide that it is worth changing/updating the pattern, there are additional steps that must be followed. See the [Maintain](/handbook/engineering/ux/ux-designer#maintain) section below for those steps.

#### Deliver

* Once your work is complete, make sure that the issue description, the SSOT, is updated with a section called "Solution". This is where you should direct people when they have questions about what should be done and how.
* As applicable, commit all design assets and files according to the instructions in the [GitLab Design project contribution guidelines][gitlab-design-project-contribution-guidelines].
* Whenever possible, provide spec-previews using the Sketch Measure plugin. Details on how to use this are located in the [design repository on the contribution page](https://gitlab.com/gitlab-org/gitlab-design/blob/master/CONTRIBUTING.md#superpowers-).
* Once UX work is completed and all feedback is addressed, remove the [UX][ux-label] label, add the [UX ready][ux-ready-label] label, and:
    * If the issue is not scheduled, mention the [responsible product manager](/handbook/product/#who-to-talk-to-for-what) to [schedule it](/handbook/engineering/workflow/#scheduling-issues)

#### Follow through

* Encourage Engineers to scope down features into multiple merge requests for an easier, more efficient review process. 
* When breaking down features into multiple merge requests, consider how the UX of the application will be affected. If merging only a portion of the total changes will negatively impact the overall experience, consider using a feature branch or feature flag to ensure that the full UX scope ships together.
* When breaking solutions into smaller parts, make sure to share the end design goal, so that the entire team has context. Giving everyone the full picture helps developers write code aimed at achieving that goal in the future.
* Keep the issue description updated with the agreed-on scope, even if doesn’t impact your work. This is everyone’s responsibility. The issue description must be the Single Source Of Truth (SSOT), not the discussion or individual comments.
* Not all issues are scheduled immediately, which means changes are likely needed when the issue is prioritized. The Product Designer responsible for a particular stage group should be aware of open issues within their product area and work to prioritize them accordingly with their respective product managers, even if they are not the original designer who worked on the issue.
    * To stay up to date with issues in your product area, subscribe to the label that matches your stage group.
    * Review issues within your stage group label regularly.
    * Actively contribute to planning meetings to ensure all open issues are being discussed and prioritized.
* When working on an issue, keep the SSOT in the description updated until the issue is closed. This applies to both text and mockups. Previous content (by a PM, for example) should be removed or archived into a separate section in the description. If the developer working on the issue ever has any questions on what they should implement, they can ask the designer to update the issue description with the design.
* For obvious changes, make the SSOT description update directly. [You don't need to wait for consensus](/handbook/values/). Use your judgement.
* When the issue is actively being worked on, make sure you are assigned and subscribed to the issue. Continue to follow both the issue and related merge request(s), addressing any additional UX issues that come up.

#### UX Reviews

* UX is part of the MR review process.
* UX Review requests are high priority. Tackle them as quickly as you are able.
* Test locally, and do not rely on screenshots.
* Be thorough. There should be as little back and forth as possible.
* If you are asked to review an MR for an issue you were not assigned to, remind the author who the assigned designer is and assign to original designer for review.
* When reviewing an MR, please use the following order of importance:
    * Functionality first: Does it work?
    * Edge cases: Are there any unexpected edge cases?
    * Visual consistency: Does it conform to our Design System and workflows in other product areas?
* Remember to stick to the issue, create issues for further updates to avoid scope creep.

#### Maintain

If the UX work introduces or changes any of the UX standards or building blocks:

* [Create an issue](https://gitlab.com/gitlab-org/gitlab-design/issues/new?issuable_template=UX%20Pattern) in the [GitLab Design Project](https://gitlab.com/gitlab-org/gitlab-design) and follow the pattern checklist to record the new standard and update the [GitLab Pattern Library Sketch file](https://gitlab.com/gitlab-org/gitlab-design/blob/master/gitlab-pattern-library.sketch).
* Once the pattern has been added to the [GitLab Design Project](https://gitlab.com/gitlab-org/gitlab-design), create an issue and MR to add the pattern and documentation to the [GitLab Design System](https://gitlab.com/gitlab-org/design.gitlab.com).
* As applicable, create issue(s) to update areas of the application that are affected by this pattern.
* Add an agenda item to the next UX weekly call to inform everyone of those changes.
* If the change/addition is a significant one, consider adding it to the Company Call to inform the company of the changes.

#### Approach

As design can be subjective, discussion can heat up. Always try to be [direct](/handbook/values/#directness) but [kind](/handbook/values/#kindness). Try to give your best reasoning for your choices, and evaluate everyone's opinions. Come up with a solution instead of discussing endlessly. If you think additional perspective is needed, mention a fellow Product Designer in the issue.

[ux-guide]: https://docs.gitlab.com/ee/development/ux_guide/
[ux-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX
[ux-ready-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX+ready
[gitlab-design-project-contribution-guidelines]: https://gitlab.com/gitlab-org/gitlab-design/blob/master/CONTRIBUTING.md
[twitter-sheet]: https://docs.google.com/spreadsheets/d/1GDAUNujD1-eRYxAj4FIYbCyy8ltCwwIWqVTd9-gf4wA/edit
[product-dev-flow]: https://about.gitlab.com/handbook/product-development-flow/
