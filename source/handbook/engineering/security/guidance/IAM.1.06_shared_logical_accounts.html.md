---
layout: markdown_page
title: "IAM.1.06 - Shared Logical Accounts Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IAM.1.06 - Shared Logical Accounts

## Control Statement

GitLab restricts the use of shared and group authentication credentials. Authentication credentials for shared and group accounts are reset quarterly.

## Context

Adding restrictions to the use of shared accounts help protect against malicious activity and stolen accounts. Shared accounts are more likely to be compromised because the account credentials are shared with multiple people and it's often not feasible to use multi-factor authentication. While shared accounts sometimes must be used, this control aims to limit account sharing to only those cases where it's necessary and no good alternatives exist, and to add safeguards to when they are used.

## Scope

TBD

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.1.06_shared_logical_accounts.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.1.06_shared_logical_accounts.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.1.06_shared_logical_accounts.md).

## Framework Mapping

* SOC2 CC
  * CC6.1
