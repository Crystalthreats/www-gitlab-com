---
layout: markdown_page
title: "IAM.4.01 - Remote Connections Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IAM.4.01 - Remote Connections

## Control Statement

Remote connections to production systems are controlled with a combination of SSH and multi-factor authentication.

## Context

Where and only if applicable, appropriate, and technically feasible, and with the understanding GitLab is a cloud-native, fully-remote and international organization favoring a Zero Trust network without a traditional corporate network infrastructure, access will be managed with a combination of [ssh](https://nvlpubs.nist.gov/nistpubs/ir/2015/NIST.IR.7966.pdf) and [Multi-factor Authentication (MFA)](https://csrc.nist.gov/glossary/term/Multi_Factor-Authentication) technologies.

## Scope

This control applies to all GitLab.com production environment systems and networks.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.4.01_remote_connections.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.4.01_remote_connections.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.4.01_remote_connections.md).

## Framework Mapping

* ISO
  * A.11.2.6
* SOC2 CC
  * CC6.6
  * CC6.7
* PCI
  * 8.1.1
  * 8.6
