---
layout: markdown_page
title: "Category Vision - Fuzzing"
---

- TOC
{:toc}

## Description
<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas, 
and user journeys into this section. -->

Fuzzing is testing technique focused on finding flaws and vulnerabilities in applications.

It consists in sending arbitrary payloads instead of valid input, in order to trigger exceptions and unintended code paths that may lead to crashes and unauthorized operations.

Given the arbitrary payload is mostly random, the attacked doesn't need to know many details about the expected input. Normally a huge amount of data is sent to the application while monitoring the effects, looking for crashes. This is what makes this attack very different from other specific attacks.

Once a possible problem (crash) has been found, attackers can focus on it, checking which are the exact conditions to trigger the bug, and if they can be fine-tuned to obtain some useful result.

Even if random data is used, normally it has some well-known structure in order to pass the first-level input validation made by the application. For example, if you want to attack a web application, the payload will be a valid HTTP request, but random data (fuzz) will be used to create parameters names and values.

## Target audience and experience
<!-- An overview of the personas involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

- [Sam](https://design.gitlab.com/research/personas/#persona-sam), Security Analyst

Fuzzing is unlikely to be useful to developers, since it is just about discovering possible flaws. Since it is also very time-consuming, it should not block the development process, but be part of a security analysis that is executed as a separate process.

This approach is primarily on the radar of very specific industries, where code is often deployed on embedded devices. For example, healthcare, automotive, electronics, and everything related to IoT.

With the category moving to maturity, it can be useful for Security Researches that want to spot vulnerabilities as their primary goal, targeting libraries and other software used by the application.

## What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

The next step is to provide a minimal support for fuzzing as part of GitLab DAST offering. Users can set up dedicated pipelines and run fuzzing sessions. This is the very minimal goal to dig into this category.

- [MVC](https://gitlab.com/groups/gitlab-org/-/epics/819)

## Maturity Plan
 - [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/818)
 
## Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

There are open source tools and a few commercial ones that do fuzzing.

A few examples:
- [OSS-Fuzz](https://github.com/google/oss-fuzz)
- [Peach Fuzzer](https://www.peach.tech/)

See [this issue](https://gitlab.com/gitlab-org/gitlab-ee/issues/10852) for a complete list.

## Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

Analysts don't have specific reports for fuzzing. The topic is sometimes discussed with their customers.
There are challenges to make it part of the DevSecOps paradigm, and they are interested to hear more about that.

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

- [MVC](https://gitlab.com/groups/gitlab-org/-/epics/819)